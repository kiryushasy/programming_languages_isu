import numpy as np
import re
file = open('cow_examples/hello.cow', 'r')
res=file.read()
stroka = re.sub(r'\n', r' ', res)
stroka = stroka.split(' ')
lst=[]
dic = {}
ind=0
result = np.zeros(1000)

for item in stroka:
    if item == 'MOO':
        lst.append(ind)
    if item == 'moo':
        dic[ind]=lst[len(lst)-1]
        dic[lst.pop()]=ind
    ind+=1
index = 0
i = 0

while(i != len(stroka)):
    if stroka[i] == 'MoO':
        result[index] += 1
    if stroka[i] == 'MOo':
        result[index] -= 1
    if stroka[i] == 'mOO':
        result[index] = i
    if stroka[i] == 'moO':
        index += 1
    if stroka[i] == 'mOo':
        index -= 1
    if stroka[i] == 'OOM':
        print(int(result[index]),end='')
    if stroka[i] == 'oom':
        result[index] = input()
    if stroka[i] == 'Moo':
        if stroka[index] != 0:
            print(chr(int(result[index])),end='')
        else:
            input()
    if stroka[i] == 'OOO':
        stroka[index] = 0
    if stroka[i] == 'moo':
        i = dic[i]-1
    if stroka[i] == 'MOO':
        if result[index] == 0:
            i = dic[i]
    if stroka[i] == '':
        pass
    else:
        i += 1
        continue
    i += 1
